import React from 'react';
import {Switch, Route} from "react-router-dom";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css'
import Navbar from './component/Navbar';
import Product from './component/Product';
import Details from './component/Details';
import ProductList from './component/ProductList';
import Default from './component/Default';
import Cart from "./component/Cart";



function App() {
  return (
      <React.Fragment>
          <Navbar/>
          <Switch>
              <Route exact path="/" component={ProductList} />
              <Route path="/details" component={Details}/>
              <Route path="/cart" component={Cart}/>
              <Route component={Default}/>
          </Switch>
          <div className="App">
              <div className="container">
                  <div className="row">
                      <div className="col-6">column</div>
                      <div className="col-6">column2 <i className="fa fa-user"></i></div>
                  </div>
              </div>
          </div>

      </React.Fragment>



  );
}

export default App;
